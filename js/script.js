let images = ['img1.png', 'img2.png', 'img3.png', 'img4.png', 'img5.png'];
let folderImgs="images";



    
$(document).ready(()=>{
    $(".slider").slick({

        // normal options...
        infinite: true,
        
        // the magic
        responsive: [{
        
            breakpoint: 1024,
            settings: {
              slidesToShow: 3,
              infinite: true
            }
        
          }, {
        
            breakpoint: 600,
            settings: {
              slidesToShow: 2,
              dots: true
            }
        
          }, {
        
            breakpoint: 300,
            settings: "unslick" // destroys slick
        
          }]
        });
    images.map(img=>{
        $('.slider').slick('slickAdd',"<div><img src='"+folderImgs+"/"+img+"'></div>");   
})
})

